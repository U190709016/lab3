import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) throws IOException {
        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        Random rand = new Random(); //Creates an object from Random class
        int number = rand.nextInt(100); //generates a number between 0 and 99
        Scanner reader = new Scanner(System.in); //Creates an object to read user input

        System.out.print("Can you guess it: ");
        int x=1;
        int z=0;

        while(x == 1) {


            int guess = reader.nextInt(); //Read the user input
            z= z+1;
            if (number == guess && guess != -1) {
                System.out.println("Congratulations! You won after " + z + " attempts!");
                x=2;
            }
            if (number > guess && guess != -1) {
                System.out.println("Sorry!");
                System.out.println("Mine ise greater than your guess.");
                x=1;
            }
            if (guess > number && guess != -1) {
                System.out.println("Sorry!");
                System.out.println("Mine ise less than your guess.");
                x=1;
            }
            if (guess == -1){
                System.out.println("Sorry the number was " + number);
                x=2;
            }

            if (x==1) {
                System.out.print("Type -1 to quit or guess another: ");
            }

        }
        reader.close(); //Close the resource before exiting

    }



}
